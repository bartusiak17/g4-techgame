import {Component} from '@angular/core';

import {Platform, PopoverController} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {AuthService} from './services/auth/auth.service';
import {AlertService} from './services/alert/alert.service';
import {PopoverLogoutComponent} from './pages/components/popover-logout/popover-logout.component';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private authService: AuthService,
        private alertService: AlertService,
        private popoverControler: PopoverController
    ) {
        this.authService.checkTokenUser();
        this.initializeApp();

    }


    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    /**
     * Déconnexion - affichage popover
     */
    logout() {
        this.popoverControler.create({component: PopoverLogoutComponent, showBackdrop: false}).then((popoverElement) => {
            popoverElement.present();
        });
    }
}
