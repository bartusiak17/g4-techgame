import {Injectable} from '@angular/core';
import {AuthService} from './services/auth/auth.service';

@Injectable({
    providedIn: 'root'
})
export class UserDataResolver {
    constructor(private authService: AuthService) {
    }

    resolve() {
        if (this.authService.tokenUser) {
            console.log('Utilisateur connecté');
        } else {
            console.log('Utilisateur deconnecté');
        }
        return this.authService.getUserData();
    }
}
