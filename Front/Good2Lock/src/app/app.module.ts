import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/storage';
import {HttpConfigInterceptor} from './provider/httpConfig.interceptor';
import {UserDataResolver} from './user-data.resolver';
import {AuthGuard} from './provider/AuthGuard/auth.guard';
import {LoginGuard} from './provider/loginGuard/login.guard';
import {AuthService} from './services/auth/auth.service';
import {HttpService} from './services/http/http.service';
import {PopoverLogoutComponent} from './pages/components/popover-logout/popover-logout.component';


@NgModule({
    declarations: [AppComponent, PopoverLogoutComponent],
    entryComponents: [PopoverLogoutComponent],
    imports: [BrowserModule,
        IonicModule.forRoot(),
        IonicStorageModule.forRoot(),
        HttpClientModule,
        AppRoutingModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        HttpClientModule,
        HttpConfigInterceptor,
        HttpService,
        AuthService,
        AuthGuard,
        LoginGuard,
        {
            provide: RouteReuseStrategy, useClass: IonicRouteStrategy
        },
        {provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true},
        UserDataResolver
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}
