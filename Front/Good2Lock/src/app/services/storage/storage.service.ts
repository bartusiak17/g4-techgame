import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor(private storage: Storage) {
    }

    // Todo : Crypter la valeur
    /**
     * Store the value
     */
    async store(storageKey: string, value: any) {
        await this.storage.set(storageKey, value);
    }

    // Todo : Décrypter la valeur
    /**
     * Get value storage
     */
    public get(storageKey: string)  {
        return this.storage.get(storageKey);
    }

    /**
     * Delete item Storage
     */
    async removeStorageItem(storageKey: string) {
        await this.storage.remove(storageKey);
    }

    /**
     *  Clear storage
     */
    async clear() {
        await this.storage.clear();
    }
}
