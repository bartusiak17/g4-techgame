import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Router} from '@angular/router';
import {StorageService} from '../storage/storage.service';
import {AuthConstants} from './authConstants';
import {HttpService} from '../http/http.service';
import {HttpClient} from '@angular/common/http';
import {UserConnexion} from '../../models/UserConnexion';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    userData$ = new BehaviorSubject<any>([]);

    tokenUser: boolean;

    token: string;

    accountValidated: boolean;

    constructor(
        private storageService: StorageService,
        private router: Router,
        private http: HttpClient,
        private httpService: HttpService
    ) {
    }

    getUserData() {
        this.storageService.get(AuthConstants.AUTH).then(res => {
            this.userData$.next(res);
        });
    }

    /**
     * Connexion
     * @param postData
     */
    login(postData: UserConnexion): Observable<any> {
        return this.httpService.loginUser(postData);
    }

    stockTokenUser(token: string) {
        this.storageService.store(AuthConstants.AUTH, token).then(res => {
            console.log('token stocké');
            this.tokenUser = true;
            this.getInfoUser();
        });
    }

    /**
     * Vérifie si l'utilisateur possède un token
     */
    checkTokenUser(): any {
        this.storageService.get(AuthConstants.AUTH).then(res => {
            if (res) {
                this.tokenUser = true;
                this.token = res;
                console.log('token check true');
                this.getInfoUser();
                return true;
            } else {
                this.tokenUser = false;
                console.log('token check false');
                return false;
            }
        });
    }

    /**
     * Supprime le Token de l'utilisateur et déconnecte l'utilisateur
     */
    logout() {
        this.storageService.removeStorageItem(AuthConstants.AUTH).then((value => {
            console.log('Token supprimé');
            this.tokenUser = false;
            this.token = null;
            this.accountValidated = null;
            this.router.navigate(['/login']);
        }));
    }


    /**
     * Récupère les informations de l'utilisateur
     */
    getInfoUser() {
        this.httpService.get('private/compte/informationsCompte').subscribe((user) => {
            this.userData$ = user;
            this.accountValidated = user.isValidatedByModo;
        });
    }


}
