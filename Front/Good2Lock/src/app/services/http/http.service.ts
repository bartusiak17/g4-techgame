import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {retry, retryWhen} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {UserConnexion} from '../../models/UserConnexion';
import {Router} from '@angular/router';


@Injectable({
    providedIn: 'root'
})
export class HttpService {

    /**
     * apiUrl
     */
    private apiUrl: string;


    /**
     * Constructor and set ApiUrl
     */
    constructor(private http: HttpClient,
                private router: Router) {
        this.apiUrl = environment.apiUrl;
    }

    /**
     * Handle Data errors
     */
    static handleErrorConnexion(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('Une erreur est survenue:', error.error.message);
            return throwError('Connexion internet requise !');
        } else if (error.status === 401) {
            return throwError('Le mot de passe entré est incorrect');
        }
        // return an observable with a user-facing error message
        return throwError(
            'Un problème est survenue. Veuillez réessayer plus tard.\n');
    }

    /**
     * Return a list of object
     */
    getListItem<T>(baseUrlData: string): Observable<T> {
        console.log(this.apiUrl + baseUrlData);
        return this.http
            .get<T>(this.apiUrl + baseUrlData)
            .pipe(
                retry(3)
            );
    }

    /**
     * Post.
     */
    post(baseUrlData: string, item): Observable<any> {
        return this.http
            .post(this.apiUrl + baseUrlData, JSON.stringify(item))
            .pipe(
                retry(1)
            );
    }

    /**
     * Post withFile
     */
    postWithFile(baseUrlData: string, item): Observable<any> {
        return this.http
            .post(this.apiUrl + baseUrlData, item)
            .pipe(
                retry(1)
            );
    }


    getWithToken(baseUrlData: string): Observable<any> {
        console.log(this.apiUrl + baseUrlData);
        return this.http
            .get(this.apiUrl + baseUrlData)
            .pipe(
                retry(3),
                retryWhen(errors => errors)
            );
    }

    /**
     * Return un object avec id
     */
    getItem<T>(url: string, id: any): Observable<T> {
        return this.http
            .get<T>(this.apiUrl + url + '/' + id)
            .pipe(
                retry(1)
            );
    }

    /**
     * Return un objet sans id
     */
    get(url: string): Observable<any> {
        return this.http
            .get(this.apiUrl + url)
            .pipe(
                retry(1)
            );
    }

    findAll() {
        return this.http
            .get('http://localhost:8000/api/private/v1/test')
            .pipe(
                retry(2)
            );
    }

    /**
     * Update object by id
     */
    updateItem<T>(id: any, item: any): Observable<T> {
        return this.http
            .put<T>(this.apiUrl + '/' + id, JSON.stringify(item))
            .pipe(
                retry(2)
            );
    }

    /**
     * Delete a object
     */
    deleteItem<T>(id: any) {
        return this.http
            .delete<T>(this.apiUrl + '/' + id)
            .pipe(
                retry(2)
            );
    }

    /**
     * Connexion
     */
    loginUser(postData: UserConnexion): Observable<UserConnexion> {
        return this.http
            .post<UserConnexion>(environment.apiUrl + 'login', JSON.stringify(postData));
    }


    testApi() {
        return this.http
            .get('http://localhost:8000/api/public/v1/test')
            .pipe(
                retry(2)
            );
    }

}
