import {Injectable} from '@angular/core';
import {AlertController} from '@ionic/angular';
import {AuthService} from '../auth/auth.service';

@Injectable({
    providedIn: 'root'
})
export class AlertService {

    constructor(public alertController: AlertController,
                private authService: AuthService) {
    }

    async presentAlert() {
        const alert = await this.alertController.create({
            header: 'Alert',
            subHeader: 'Subtitle',
            message: 'This is an alert message.',
            buttons: ['OK']
        });

        await alert.present();
    }

    async presentAlertConfirm() {
        const alert = await this.alertController.create({
            message: '<div class="text-center">' +
                '<img alt="deconnexion" src="assets/pictures/logout.png"/>' +
                '<br> <br> <br>' +
                '<p class="mt-2 font-weight-bold" style="font-size: 16px;color: #707070;font-family: Segoe UI">' +
                'Etes-vous sur?</p></div>',
            cssClass: 'alertLogout',
            buttons: [
                {
                    text: 'Annuler',
                    cssClass: 'valid',
                    role: 'cancel',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Confirmer',
                    handler: () => {
                        this.authService.logout();
                    }
                }
            ]
        });

        await alert.present();
    }

    logout() {
        alert('test');
    }
}
