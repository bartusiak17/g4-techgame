import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';

import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {StorageService} from '../services/storage/storage.service';
import {Router} from '@angular/router';
import {ToastService} from '../services/toast/toast.service';
import {AuthService} from '../services/auth/auth.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {


    constructor(private storageService: StorageService,
                private router: Router,
                private toastService: ToastService,
                private authService: AuthService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
       /* let headers = request.headers.set('Content-type', 'application/json');
        if (this.authService.token) {
            headers = request.headers.set('Content-type', 'application/json').append('Authorization',
                `Bearer ${this.authService.token}`);
            headers.append('Content-Type', 'multipart/form-data');


        }
        request = request.clone({
            headers
        });*/
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    // Todo : Uniquement pour le dev
                    console.log('event--->>>', event);
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                console.log(error);
                const errorMessage = error.error.message;
                if (error.error instanceof ErrorEvent) {
                    // A client-side or network error occurred. Handle it accordingly.
                    console.error('Une erreur est survenue:', error.error.message);
                    return throwError('Connexion internet requise !');
                }
                if (error.status === 401) {
                    if (errorMessage === 'Invalid credentials.') {
                        return throwError(' L’adresse e-mail ou le mot de passe que vous avez entré n’est pas valide');
                    } else if (errorMessage === 'JWT Token not found' && (this.authService.token === null || this.authService.token === undefined)) {
                        this.router.navigate(['login']);
                        this.toastService.presentToast('Merci de vous connecter');
                        /*
                                                return throwError('Vous n\'avez pas accès à cette ressource');
                        */
                    } else if (errorMessage === 'Expired JWT Token') {
                        this.authService.logout();
                        this.toastService.presentToast('Merci de vous reconnecter');
                        return throwError('Merci de vous reconnecter');
                    }
                }
                if (error.status === 0) {
                    this.toastService.presentToast('Un problème est survenue. Veuillez réessayer plus tard.');
                }
                console.error(
                    `Le Backend viens de retourner le code suivant : ${error.status}. \n` +
                    `Le corps était: ${error.error}`);
                // return an observable with a user-facing error message
                return throwError(
                    'Un problème est survenue. Veuillez réessayer plus tard.\n');
            }));
    }
}
