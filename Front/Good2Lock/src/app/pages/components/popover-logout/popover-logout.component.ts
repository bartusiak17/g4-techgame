import {Component, OnInit} from '@angular/core';
import {Events, NavParams, PopoverController} from '@ionic/angular';
import {AuthService} from '../../../services/auth/auth.service';


@Component({
    selector: 'app-popover-logout',
    templateUrl: './popover-logout.component.html',
    styleUrls: ['./popover-logout.component.scss'],
})
export class PopoverLogoutComponent implements OnInit {

    constructor(public popoverController: PopoverController,
                private authService: AuthService) {
    }

    ngOnInit() {
    }

    /**
     * Ferme le popover
     */
    dismissPopover() {
        this.popoverController.dismiss();
    }

    /**
     * Déconnection de l'utilisateur
     */
    logout() {
        this.authService.logout();
        this.dismissPopover();
    }

}
