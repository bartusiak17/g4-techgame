import {Component, OnInit} from '@angular/core';
import {timeInterval} from 'rxjs/operators';

@Component({
    selector: 'app-tchat',
    templateUrl: './tchat.page.html',
    styleUrls: ['./tchat.page.scss'],
})
export class TchatPage implements OnInit {

    list: string[] = ['message1', 'message2'];

    constructor() {
    }

    ngOnInit() {

    }
    push(){
        this.list.push('message3');

    }

}
