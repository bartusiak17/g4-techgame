import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserRegisterService} from '../service/user-register.service';
import {UserInscription} from '../model/UserInscription';
import {HttpService} from '../../../services/http/http.service';
import {ToastService} from '../../../services/toast/toast.service';
import {SponsorshipeInscription} from '../model/sponsorshipeInscription';

@Component({
    selector: 'app-register-step3',
    templateUrl: './register-step3.component.html',
    styleUrls: ['./register-step3.component.scss'],
})

export class RegisterStep3Component implements OnInit {
    formRegisterStep3: FormGroup;

    userInscription = new UserInscription();


    constructor(private router: Router,
                private formBuilder: FormBuilder,
                private userRegisterService: UserRegisterService,
                private httpService: HttpService,
                private toastService: ToastService) {
    }

    ngOnInit() {
        this.userInscription = this.userRegisterService.getUserInscription();
        this.initForm();
    }

    /**
     * Register Form step 3
     */
    initForm() {
        this.formRegisterStep3 = this.formBuilder.group({
            sponsorshipcode: new FormControl('', Validators.compose([
                    Validators.minLength(5),
                    Validators.maxLength(5)
                ])
            ),
            password: new FormControl('', Validators.compose([
                    Validators.required,
                    Validators.minLength(5),
                    Validators.maxLength(30)
                ])
            ),
            passwordConfirmation: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(30),
            ]))
        }, {
            validators: this.confirmPassword.bind(this)
        });
    }

    /**
     * Vérifie les mots de passe
     * @param formGroup
     */
    confirmPassword(formGroup: FormGroup) {
        const {value: password} = formGroup.get('password');
        const {value: password2} = formGroup.get('passwordConfirmation');
        return password === password2 ? null : {passwordNotMatch: true};
    }


    /**
     * Submit le formulaire et vérifie le code de parrainage si il est rempli
     */
    onSubmit() {
        if (!this.formRegisterStep3.valid) {
            return;
        }
        if (this.formRegisterStep3.get('sponsorshipcode').value !== '') {
            const sponsorshipeInscription = new SponsorshipeInscription(this.formRegisterStep3.get('sponsorshipcode').value);
            this.httpService.post('public/compte/verifier/codeparrainage', sponsorshipeInscription).subscribe(
                (res) => {
                    if (res.message === 'ok') {
                        this.userInscription.password = this.formRegisterStep3.get('password').value;
                        this.userRegisterService.setUserInscription(this.userInscription);
                        this.router.navigate(['inscription/etape/4']);
                    } else {
                        this.toastService.presentToast(res.message);
                    }
                },
                (error: any) => {
                    this.toastService.presentToast(error);
                });
        } else {
            this.userInscription.password = this.formRegisterStep3.get('password').value;
            this.userRegisterService.setUserInscription(this.userInscription);
            this.router.navigate(['inscription/etape/4']);
        }

    }

}
