import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {RegisterPageRoutingModule} from './register-routing.module';

import {RegisterPage} from './register.page';
import {RegisterStep2Component} from './register-step2/register-step2.component';
import {RegisterStep3Component} from './register-step3/register-step3.component';
import {RegisterStep4Component} from './register-step4/register-step4.component';
import {RegisterStep5Component} from './register-step5/register-step5.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RegisterPageRoutingModule,
        ReactiveFormsModule
    ],
    declarations: [RegisterPage, RegisterStep2Component, RegisterStep3Component, RegisterStep4Component, RegisterStep5Component]
})
export class RegisterPageModule {
}
