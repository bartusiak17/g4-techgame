import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastService} from '../../services/toast/toast.service';
import {HttpService} from '../../services/http/http.service';
import {UserInscription} from './model/UserInscription';
import {UserRegisterService} from './service/user-register.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
    formRegister: FormGroup;

    userInscription = new UserInscription();


    constructor(private router: Router,
                private formBuilder: FormBuilder,
                private toastService: ToastService,
                private httpService: HttpService,
                private userRegisterService: UserRegisterService) {
    }

    ngOnInit() {
        this.initForm();
    }

    /**
     * Register Form
     */
    initForm() {
        this.formRegister = this.formBuilder.group({
            lastName: new FormControl('', Validators.compose([
                    Validators.required,
                    Validators.minLength(6),
                    Validators.maxLength(50)
                ])
            ),
            firstName: new FormControl('', Validators.compose([
                    Validators.required,
                    Validators.minLength(5),
                    Validators.maxLength(30)
                ])
            ),
            email: new FormControl('', Validators.compose([
                    Validators.required,
                    Validators.minLength(6),
                    Validators.maxLength(50),
                    Validators.pattern('[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+.[a-zA-Z.]{2,15}')
                ])
            )
        });
    }

    onSubmit() {
        if (!this.formRegister.valid) {
            return;
        }
        this.userInscription.email = this.formRegister.get('email').value;
        this.userInscription.lastName = this.formRegister.get('lastName').value;
        this.userInscription.firstName = this.formRegister.get('firstName').value;
        this.httpService.post('public/compte/verifier/email', this.userInscription).subscribe(
            (res) => {
                if (res.message === 'ok') {
                    this.userRegisterService.setUserInscription(this.userInscription);
                    this.router.navigate(['inscription/etape/2']);
                } else {
                    this.toastService.presentToast(res.message);
                }
            },
            (error: any) => {
                this.toastService.presentToast(error);
            });
    }
}


