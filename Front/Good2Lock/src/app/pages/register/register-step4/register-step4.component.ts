import {Component, OnInit} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {ActionSheetController} from '@ionic/angular';
import {UserRegisterService} from '../service/user-register.service';
import {CameraResultType, CameraSource, Plugins} from '@capacitor/core';
import {UserInscription} from '../model/UserInscription';

@Component({
    selector: 'app-register-step4',
    templateUrl: './register-step4.component.html',
    styleUrls: ['./register-step4.component.scss'],
})
export class RegisterStep4Component implements OnInit {

    picture: SafeResourceUrl;

    userInscription = new UserInscription();

    constructor(private sanitizer: DomSanitizer,
                private actionSheetController: ActionSheetController,
                private userRegisterService: UserRegisterService) {
    }

    ngOnInit() {
    }


    /**
     * Affiche la liste de choix
     */
    async presentActionSheet() {
        const actionSheet = await this.actionSheetController.create({
            buttons: [{
                text: 'Prendre une photo',
                icon: 'camera',
                handler: () => {
                    this.takePicture();
                }
            }, {
                text: 'Mes fichiers',
                icon: 'folder',
                handler: () => {
                    console.log('Folder clicked');
                }
            }]
        });
        await actionSheet.present();
    }

    /**
     * Prend une photo
     */
    async takePicture() {
        const image = await Plugins.Camera.getPhoto({
            quality: 100,
            allowEditing: false,
            resultType: CameraResultType.DataUrl,
            source: CameraSource.Camera
        });
        this.picture = this.sanitizer.bypassSecurityTrustResourceUrl(image && (image.dataUrl));
    }


    /**
     * Todo à terminé une fois la gestion d'image réalisé
     */
    onSubmit() {
        console.log('onSubmit');
        this.inscription();
    }

    /**
     * Inscription de l'utilisateur et envoie email
     */
    inscription() {
        this.userRegisterService.inscriptionUser();
    }
}
