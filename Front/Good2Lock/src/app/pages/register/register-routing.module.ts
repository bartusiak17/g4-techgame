import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterPage } from './register.page';
import {RegisterStep2Component} from './register-step2/register-step2.component';
import {RegisterStep3Component} from './register-step3/register-step3.component';
import {RegisterStep4Component} from './register-step4/register-step4.component';
import {RegisterStep5Component} from './register-step5/register-step5.component';

const routes: Routes = [
  {
    path: '',
    component: RegisterPage
  },
  {
    path: 'etape/2',
    component: RegisterStep2Component
  },
  {
    path: 'etape/3',
    component: RegisterStep3Component
  },
  {
    path: 'etape/4',
    component: RegisterStep4Component
  },
  {
    path: 'etape/5',
    component: RegisterStep5Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterPageRoutingModule {}
