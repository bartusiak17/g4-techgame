import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserRegisterService} from '../service/user-register.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserInscription} from '../model/UserInscription';

@Component({
    selector: 'app-register-step2',
    templateUrl: './register-step2.component.html',
    styleUrls: ['./register-step2.component.scss'],
})
export class RegisterStep2Component implements OnInit {
    formRegisterStep2: FormGroup;

    userInscription = new UserInscription();

    constructor(private router: Router,
                private userRegisterService: UserRegisterService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.userInscription = this.userRegisterService.getUserInscription();
        this.initForm();
    }


    /**
     * Register Form step 2
     */
    initForm() {
        const patternTel = '^((\\+\\d{1,3}(-| )?\\(?\\d\\)?(-| )?\\d{1,5})|(\\(?\\d{2,6}\\)?))(-| )?(\\d{3,4})(-| )?(\\d{4})(( x| ext)\\d{1,5}){0,1}$';
        this.formRegisterStep2 = this.formBuilder.group({
            zipCode: new FormControl('', Validators.compose([
                    Validators.required,
                    Validators.minLength(5),
                    Validators.maxLength(5)
                ])
            ),
            city: new FormControl('', Validators.compose([
                    Validators.required,
                    Validators.minLength(5),
                    Validators.maxLength(30)
                ])
            ),
            tel: new FormControl('', Validators.compose([
                    Validators.required,
                    Validators.pattern(patternTel)
                ])
            )
        });
    }


    onSubmit() {
        this.router.navigate(['inscription/etape/3']);

        if (!this.formRegisterStep2.valid) {
            return;
        }
        this.userInscription.zipCode = this.formRegisterStep2.get('zipCode').value;
        this.userInscription.city = this.formRegisterStep2.get('city').value;
        this.userInscription.tel = this.formRegisterStep2.get('tel').value;
        this.userRegisterService.setUserInscription(this.userInscription);
    }

}
