import {Injectable} from '@angular/core';
import {UserInscription} from '../model/UserInscription';
import {HttpService} from '../../../services/http/http.service';
import {ToastService} from '../../../services/toast/toast.service';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class UserRegisterService {

    userInscription = new UserInscription();

    constructor(private httpService: HttpService,
                private toastService: ToastService,
                private router: Router) {
    }


    getUserInscription() {
        return this.userInscription;
    }

    setUserInscription(userInscription: UserInscription) {
        this.userInscription = userInscription;
    }

    inscriptionUser() {
        if (this.getUserInscription()) {
            this.userInscription.adress = 'test';
            this.httpService.post('public/compte/inscription', this.getUserInscription()).subscribe(
                (res) => {
                    console.log(res.message);
                    if (res.message === 'ok') {
                        this.toastService.presentToast('Utilisateur Inscrit');
                        this.router.navigate(['inscription/etape/5']);
                    } else {
                        this.toastService.presentToast('Erreur');
                    }
                },
                (error: any) => {
                    this.toastService.presentToast(error);
                });
        }
    }
}
