import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';


import {HomePage} from './home.page';
import {HomePageRoutingModule} from './home-routing.module';
import {SharedModuleModule} from '../../shared-module/shared-module.module';

@NgModule({
    entryComponents: [],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HomePageRoutingModule,
        SharedModuleModule,
        ReactiveFormsModule
    ],
    declarations: [HomePage]
})
export class HomePageModule {
}
