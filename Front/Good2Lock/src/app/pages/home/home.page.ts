import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../services/http/http.service';
import {AuthService} from '../../services/auth/auth.service';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {filetest} from '../../models/filetest';
import {Observable} from 'rxjs';


export class AdConsult {
    public libelle: string;
    public categorie: string;
    public images: Array<string>;
    public auteur: string;
    public prix: number;
    public description: string;
}

/*
class ImageSnippet {
    pending: boolean = false;
    status: string = 'init';

    constructor(public src: string, public file: File) {
    }
}
*/


@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {


    formFile: FormGroup;
    fileData: File = null;
    /* processFile(imageInput: any) {
         const file: File = imageInput.files[0];
         const reader = new FileReader();
         const fileList: FileList = event.target.files;
         if(fileList.length > 0) {
             let file: File = fileList[0];
             let formData:FormData = new FormData();
             formData.append('uploadFile', file, file.name);
             let options = new RequestOptions({ headers: headers });
             this.http.post(`${this.apiEndPoint}`, formData, options)
                 .map(res => res.json())
                 .catch(error => Observable.throw(error))
                 .subscribe(
                     data => console.log('success'),
                     error => console.log(error)
                 )
         }

         reader.addEventListener('load', (event: any) => {
             this.uploadImage(file).subscribe(
                 (res) => {
                     console.log(res);
                 },
                 (err) => {

                 });
         });

         reader.readAsDataURL(file);
     }
 */
    previewUrl: any = null;

    constructor(private httpService: HttpService,
                private authService: AuthService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        /*
                this.initForm();
        */
    }


    /**
     * Register Form
     */
    /*initForm() {
        this.formFile = this.formBuilder.group({
            file: new FormControl('')

        });
    }

    testWithToken() {
        this.httpService.findAll().subscribe((res) => {
            if (res) {
                console.log(res);
            }
        });
    }


    fileChange(fileInput: FileList) {
        this.fileData = fileInput.item(0);
    }*/


    /* const fileList: FileList = event.target.files;
     if (fileList.length > 0) {
         const file: File = fileList[0];
         const formData: FormData = new FormData();
         this.selectedFile = new filetest(file);
         formData.append('uploadFile', file, file.name);
         this.httpService.post(`public/test/blob`, this.selectedFile.file).subscribe((res) => {
             console.log(res.json);
         });
     }*/


    /*uploadImage(image: File): Observable<Response> {
        const formData = new FormData();

        formData.append('image', image);

        console.log(formData);
        return this.httpService.post('public/test/blob', formData);
    }


    onSubmit() {
        const formData = new FormData();
        const monObjet = {
            file: formData.append('file', this.fileData, this.fileData.name)
        };
        console.log(monObjet.file);
        this.httpService.postWithFile('public/test/blob', monObjet)
            .subscribe(res => {
                console.log(res);
            });
    }*/




}
