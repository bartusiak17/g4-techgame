import {Component, OnInit} from '@angular/core';
import {ToastService} from '../../services/toast/toast.service';
import {StorageService} from '../../services/storage/storage.service';
import {AuthService} from '../../services/auth/auth.service';
import {Router} from '@angular/router';
import {UserConnexion} from '../../models/UserConnexion';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    formLogin: FormGroup;

    type = 'password';

    iconName = 'eye-off';

    constructor(
        private router: Router,
        private authService: AuthService,
        private storageService: StorageService,
        private toastService: ToastService,
        public formBuilder: FormBuilder
    ) {
    }

    ngOnInit() {
        this.initForm();
    }

    /**
     * Login Form
     */
    initForm() {
        this.formLogin = this.formBuilder.group({
            email: new FormControl('', Validators.compose([
                    Validators.required,
                    Validators.minLength(5),
                    Validators.maxLength(30),
                    Validators.pattern('[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+.[a-zA-Z.]{2,15}')
                ])
            ),
            password: new FormControl('', Validators.compose([
                    Validators.required,
                    Validators.minLength(6),
                    Validators.maxLength(50)
                ])
            )
        });
    }

    /**
     * Valide le formulaire et connecte le user en cas de succès
     */
    onSubmit() {
        if (this.formLogin.valid) {
            const userConnexion = new UserConnexion(this.formLogin.get('email').value, this.formLogin.get('password').value);
            this.authService.login(userConnexion).subscribe(
                (res) => {
                    if (res) {
                        this.authService.token = res.token;
                        this.authService.stockTokenUser(res.token);
                        this.authService.tokenUser = true;
                        this.toastService.presentToast('Authentification réussie');
                        this.router.navigate(['']);
                    }
                },
                (error: any) => {
                    this.toastService.presentToast(error);
                }
            );
        }
    }

    /**
     * Affiche le mot de passe où le masque. Changement d'icone
     */
    showPassword() {
        if (this.type === 'password') {
            this.iconName = 'eye';
            this.type = 'text';
        } else {
            this.iconName = 'eye-off';
            this.type = 'password';
        }
    }


    /**
     * Méthode mot de passe oublié
     */
    forgetPassword() {
        alert('mot de passe oublié');
    }

}
