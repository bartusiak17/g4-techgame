import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UsersPasswordForget} from '../../../models/UsersPasswordForget';
import {$t} from 'codelyzer/angular/styles/chars';
import {HttpService} from '../../../services/http/http.service';
import {AuthService} from '../../../services/auth/auth.service';
import {ToastService} from '../../../services/toast/toast.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-forgetpassword',
    templateUrl: './forgetpassword.page.html',
    styleUrls: ['./forgetpassword.page.scss'],
})
export class ForgetpasswordPage implements OnInit {

    formForgetPassword: FormGroup;


    constructor(private formBuilder: FormBuilder,
                private httpService: HttpService,
                private toastService: ToastService,
                private router: Router) {
    }


    ngOnInit() {
        this.initForm();
    }

    /**
     * ForgetPassword Form
     */
    initForm() {
        this.formForgetPassword = this.formBuilder.group({
            email: new FormControl('', Validators.compose([
                    Validators.required,
                    Validators.minLength(5),
                    Validators.maxLength(30),
                    Validators.pattern('[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+.[a-zA-Z.]{2,15}')
                ])
            )
        });
    }

    onSubmit() {
        if (this.formForgetPassword.valid) {
            const userPasswordForget = new UsersPasswordForget(this.formForgetPassword.get('email').value);
            this.httpService.post('public/register/forget/password', userPasswordForget).subscribe(
                (res) => {
                    if (res.message === 'ok') {
                        this.toastService.presentToast('Un nouveau mot de passe vous a été envoyé sur votre boite mail');
                        this.router.navigate(['login']);
                    } else if (res.message === 'utilisateur inconnu') {
                        this.toastService.presentToast('L\'adresse mail est inconnu');
                    }
                }
            );
        }

    }

}
