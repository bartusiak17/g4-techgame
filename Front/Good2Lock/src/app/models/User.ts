export class User {

    constructor(private id: number,
                private firstName: string,
                private lastName: string,
                private password: string,
                private token: string) {
    }
}
