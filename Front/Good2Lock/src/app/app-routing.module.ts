import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {LoginGuard} from './provider/loginGuard/login.guard';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
    },
    {
        path: 'login',
        loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule),
        canActivate: [LoginGuard]
    },
    {
        path: 'connexionInscription',
        loadChildren: () => import('./pages/login-register/login-register.module').then(m => m.LoginRegisterPageModule),
        canActivate: [LoginGuard]
    }, {
        path: 'inscription',
        loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterPageModule),
        canActivate: [LoginGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
