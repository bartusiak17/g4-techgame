import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AccountVerifyComponent} from '../pages/components/account-verify/account-verify.component';



@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AccountVerifyComponent],
  exports: [AccountVerifyComponent]
})
export class SharedModuleModule { }
