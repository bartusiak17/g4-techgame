<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PicturesAdRepository")
 */
class PicturesAd
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ad", inversedBy="id_pictures_ad")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_ad;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getIdAd(): ?Ad
    {
        return $this->id_ad;
    }

    public function setIdAd(?Ad $id_ad): self
    {
        $this->id_ad = $id_ad;

        return $this;
    }
}
