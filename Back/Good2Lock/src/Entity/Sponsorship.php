<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SponsorshipRepository")
 */
class Sponsorship
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $crated_at;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_activated;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_user1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_user2;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCratedAt(): ?\DateTimeInterface
    {
        return $this->crated_at;
    }

    public function setCratedAt(\DateTimeInterface $crated_at): self
    {
        $this->crated_at = $crated_at;

        return $this;
    }

    public function getIsActivated(): ?bool
    {
        return $this->is_activated;
    }

    public function setIsActivated(bool $is_activated): self
    {
        $this->is_activated = $is_activated;

        return $this;
    }

    public function getIdUser1(): ?User
    {
        return $this->id_user1;
    }

    public function setIdUser1(?User $id_user1): self
    {
        $this->id_user1 = $id_user1;

        return $this;
    }

    public function getIdUser2(): ?User
    {
        return $this->id_user2;
    }

    public function setIdUser2(?User $id_user2): self
    {
        $this->id_user2 = $id_user2;

        return $this;
    }
}
