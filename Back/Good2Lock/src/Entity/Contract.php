<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContractRepository")
 */
class Contract
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $start_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $end_date;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $start_adress;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $end_adress;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $object;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ZipCode")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_zipcode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_city;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_user1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_user2;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Conversations", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_conversation;



    public function __construct()
    {
        }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->start_date;
    }

    public function setStartDate(\DateTimeInterface $start_date): self
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(\DateTimeInterface $end_date): self
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStartAdress(): ?string
    {
        return $this->start_adress;
    }

    public function setStartAdress(string $start_adress): self
    {
        $this->start_adress = $start_adress;

        return $this;
    }

    public function getEndAdress(): ?string
    {
        return $this->end_adress;
    }

    public function setEndAdress(string $end_adress): self
    {
        $this->end_adress = $end_adress;

        return $this;
    }

    public function getObject(): ?string
    {
        return $this->object;
    }

    public function setObject(string $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function getIdZipcode(): ?ZipCode
    {
        return $this->id_zipcode;
    }

    public function setIdZipcode(?ZipCode $id_zipcode): self
    {
        $this->id_zipcode = $id_zipcode;

        return $this;
    }

    public function getIdCity(): ?City
    {
        return $this->id_city;
    }

    public function setIdCity(?City $id_city): self
    {
        $this->id_city = $id_city;

        return $this;
    }

    public function getIdUser1(): ?User
    {
        return $this->id_user1;
    }

    public function setIdUser1(?User $id_user1): self
    {
        $this->id_user1 = $id_user1;

        return $this;
    }

    public function getIdUser2(): ?User
    {
        return $this->id_user2;
    }

    public function setIdUser2(?User $id_user2): self
    {
        $this->id_user2 = $id_user2;

        return $this;
    }

    public function getIdConversation(): ?Conversations
    {
        return $this->id_conversation;
    }

    public function setIdConversation(Conversations $id_conversation): self
    {
        $this->id_conversation = $id_conversation;

        return $this;
    }


}
