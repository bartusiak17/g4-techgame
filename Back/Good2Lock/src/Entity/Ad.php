<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdRepository")
 */
class Ad
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_activated;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_validated_by_modo;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="datetime")
     */
    private $start_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $end_date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PicturesAd", mappedBy="id_ad")
     */
    private $id_pictures_ad;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SubCategorie")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_subcategorie;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ZipCode")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_zipCode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_city;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UnavailableDate", mappedBy="ad")
     */
    private $id_unavailable_date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $id_modo_validated;

    public function __construct()
    {
        $this->id_pictures_ad = new ArrayCollection();
        $this->id_unavailable_date = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNote(): ?float
    {
        return $this->note;
    }

    public function setNote(float $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getIsActivated(): ?bool
    {
        return $this->is_activated;
    }

    public function setIsActivated(bool $is_activated): self
    {
        $this->is_activated = $is_activated;

        return $this;
    }

    public function getIsValidatedByModo(): ?bool
    {
        return $this->is_validated_by_modo;
    }

    public function setIsValidatedByModo(bool $is_validated_by_modo): self
    {
        $this->is_validated_by_modo = $is_validated_by_modo;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->start_date;
    }

    public function setStartDate(\DateTimeInterface $start_date): self
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(\DateTimeInterface $end_date): self
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getIdUser(): ?User
    {
        return $this->id_user;
    }

    public function setIdUser(?User $id_user): self
    {
        $this->id_user = $id_user;

        return $this;
    }

    /**
     * @return Collection|PicturesAd[]
     */
    public function getIdPicturesAd(): Collection
    {
        return $this->id_pictures_ad;
    }

    public function addIdPicturesAd(PicturesAd $idPicturesAd): self
    {
        if (!$this->id_pictures_ad->contains($idPicturesAd)) {
            $this->id_pictures_ad[] = $idPicturesAd;
            $idPicturesAd->setIdAd($this);
        }

        return $this;
    }

    public function removeIdPicturesAd(PicturesAd $idPicturesAd): self
    {
        if ($this->id_pictures_ad->contains($idPicturesAd)) {
            $this->id_pictures_ad->removeElement($idPicturesAd);
            // set the owning side to null (unless already changed)
            if ($idPicturesAd->getIdAd() === $this) {
                $idPicturesAd->setIdAd(null);
            }
        }

        return $this;
    }

    public function getIdSubcategorie(): ?SubCategorie
    {
        return $this->id_subcategorie;
    }

    public function setIdSubcategorie(?SubCategorie $id_subcategorie): self
    {
        $this->id_subcategorie = $id_subcategorie;

        return $this;
    }

    public function getIdZipCode(): ?ZipCode
    {
        return $this->id_zipCode;
    }

    public function setIdZipCode(?ZipCode $id_zipCode): self
    {
        $this->id_zipCode = $id_zipCode;

        return $this;
    }

    public function getIdCity(): ?City
    {
        return $this->id_city;
    }

    public function setIdCity(?City $id_city): self
    {
        $this->id_city = $id_city;

        return $this;
    }

    /**
     * @return Collection|UnavailableDate[]
     */
    public function getIdUnavailableDate(): Collection
    {
        return $this->id_unavailable_date;
    }

    public function addIdUnavailableDate(UnavailableDate $idUnavailableDate): self
    {
        if (!$this->id_unavailable_date->contains($idUnavailableDate)) {
            $this->id_unavailable_date[] = $idUnavailableDate;
            $idUnavailableDate->setAd($this);
        }

        return $this;
    }

    public function removeIdUnavailableDate(UnavailableDate $idUnavailableDate): self
    {
        if ($this->id_unavailable_date->contains($idUnavailableDate)) {
            $this->id_unavailable_date->removeElement($idUnavailableDate);
            // set the owning side to null (unless already changed)
            if ($idUnavailableDate->getAd() === $this) {
                $idUnavailableDate->setAd(null);
            }
        }

        return $this;
    }

    public function getIdModoValidated(): ?User
    {
        return $this->id_modo_validated;
    }

    public function setIdModoValidated(?User $id_modo_validated): self
    {
        $this->id_modo_validated = $id_modo_validated;

        return $this;
    }
}
