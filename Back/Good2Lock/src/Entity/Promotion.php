<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PromotionRepository")
 */
class Promotion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_activated;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $duration;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $euro_price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $virtual_price;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsActivated(): ?bool
    {
        return $this->is_activated;
    }

    public function setIsActivated(bool $is_activated): self
    {
        $this->is_activated = $is_activated;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEuroPrice(): ?float
    {
        return $this->euro_price;
    }

    public function setEuroPrice(?float $euro_price): self
    {
        $this->euro_price = $euro_price;

        return $this;
    }

    public function getVirtualPrice(): ?float
    {
        return $this->virtual_price;
    }

    public function setVirtualPrice(?float $virtual_price): self
    {
        $this->virtual_price = $virtual_price;

        return $this;
    }
}
