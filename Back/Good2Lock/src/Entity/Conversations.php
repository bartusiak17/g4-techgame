<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConversationsRepository")
 */
class Conversations
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $demand_validated;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_activated;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_user1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_user2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ad")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_ad;





    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDemandValidated(): ?bool
    {
        return $this->demand_validated;
    }

    public function setDemandValidated(bool $demand_validated): self
    {
        $this->demand_validated = $demand_validated;

        return $this;
    }

    public function getIsActivated(): ?bool
    {
        return $this->is_activated;
    }

    public function setIsActivated(bool $is_activated): self
    {
        $this->is_activated = $is_activated;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getIdUser1(): ?User
    {
        return $this->id_user1;
    }

    public function setIdUser1(?User $id_user1): self
    {
        $this->id_user1 = $id_user1;

        return $this;
    }

    public function getIdUser2(): ?User
    {
        return $this->id_user2;
    }

    public function setIdUser2(?User $id_user2): self
    {
        $this->id_user2 = $id_user2;

        return $this;
    }

    public function getIdAd(): ?Ad
    {
        return $this->id_ad;
    }

    public function setIdAd(?Ad $id_ad): self
    {
        $this->id_ad = $id_ad;

        return $this;
    }




}
