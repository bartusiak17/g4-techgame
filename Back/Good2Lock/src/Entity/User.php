<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $lastName;

    /**
     * @ORM\Column(type="integer")
     */
    private $creditsNumber;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adress;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_validated_by_modo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_deleted;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_activated;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $sponsorship_code;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ZipCode")
     * @ORM\JoinColumn(nullable=false)
     */
    private $zipCode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;


    public function __construct()
    {
    }


    public function getId(): ?int {
        return $this->id;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     * @see UserInterface
     */
    public function getUsername(): string {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string {
        return (string) $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt() {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials() {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self {
        $this->lastName = $lastName;

        return $this;
    }

    public function getCreditsNumber(): ?int {
        return $this->creditsNumber;
    }

    public function setCreditsNumber(int $creditsNumber): self {
        $this->creditsNumber = $creditsNumber;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self {
        $this->created_at = $created_at;

        return $this;
    }

    public function getTel(): ?string {
        return $this->tel;
    }

    public function setTel(string $tel): self {
        $this->tel = $tel;

        return $this;
    }

    public function getAdress(): ?string {
        return $this->adress;
    }

    public function setAdress(string $adress): self {
        $this->adress = $adress;

        return $this;
    }

    public function getNote(): ?float {
        return $this->note;
    }

    public function setNote(float $note): self {
        $this->note = $note;

        return $this;
    }

    public function getIsValidatedByModo(): ?bool {
        return $this->is_validated_by_modo;
    }

    public function setIsValidatedByModo(bool $is_validated_by_modo): self {
        $this->is_validated_by_modo = $is_validated_by_modo;

        return $this;
    }

    public function getIsDeleted(): ?bool {
        return $this->is_deleted;
    }

    public function setIsDeleted(bool $is_deleted): self {
        $this->is_deleted = $is_deleted;

        return $this;
    }

    public function getIsActivated(): ?bool {
        return $this->is_activated;
    }

    public function setIsActivated(bool $is_activated): self {
        $this->is_activated = $is_activated;

        return $this;
    }

    public function getPicture(): ?string {
        return $this->picture;
    }

    public function setPicture(?string $picture): self {
        $this->picture = $picture;

        return $this;
    }

    public function getSponsorshipCode(): ?string {
        return $this->sponsorship_code;
    }

    public function setSponsorshipCode(string $sponsorship_code): self {
        $this->sponsorship_code = $sponsorship_code;

        return $this;
    }

    public function getZipCode(): ?ZipCode {
        return $this->zipCode;
    }

    public function setZipCode(?ZipCode $zipCode): self {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCity(): ?City {
        return $this->city;
    }

    public function setCity(?City $city): self {
        $this->city = $city;

        return $this;
    }
}
