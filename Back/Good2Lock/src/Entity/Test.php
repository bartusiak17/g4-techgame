<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TestRepository")
 */
class Test
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $created_ad;

    /**
     * @ORM\Column(type="time")
     */
    private $testdate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAd(): ?\DateTimeInterface
    {
        return $this->created_ad;
    }

    public function setCreatedAd(\DateTimeInterface $created_ad): self
    {
        $this->created_ad = $created_ad;

        return $this;
    }

    public function getTestdate(): ?\DateTimeInterface
    {
        return $this->testdate;
    }

    public function setTestdate(\DateTimeInterface $testdate): self
    {
        $this->testdate = $testdate;

        return $this;
    }
}
