<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BlobTestRepository")
 */
class BlobTest
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="blob")
     */
    private $blobtest;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBlobtest()
    {
        return $this->blobtest;
    }

    public function setBlobtest($blobtest): self
    {
        $this->blobtest = $blobtest;

        return $this;
    }
}
