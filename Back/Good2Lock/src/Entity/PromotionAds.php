<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PromotionAdsRepository")
 */
class PromotionAds
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $end_date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Promotion")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_promotion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ad")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_ad;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(?\DateTimeInterface $end_date): self
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getIdPromotion(): ?Promotion
    {
        return $this->id_promotion;
    }

    public function setIdPromotion(?Promotion $id_promotion): self
    {
        $this->id_promotion = $id_promotion;

        return $this;
    }

    public function getIdAd(): ?Ad
    {
        return $this->id_ad;
    }

    public function setIdAd(?Ad $id_ad): self
    {
        $this->id_ad = $id_ad;

        return $this;
    }
}
