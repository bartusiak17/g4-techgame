<?php

namespace App\Controller\Api\Visiteur\Register;

use App\Repository\UserRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/v1/public/register")
 * Class ForgetPasswordController
 * @package App\Controller\Api\Visiteur\Register
 */
class ForgetPasswordController extends AbstractFOSRestController {


    /**
     * Méthode pour générer un nouveau mot de passe à l'utilisateur en cas d'oublis.
     * @Rest\Post("/forget/password", name="api_visiteur_register_forget_password")
     * @throws \Exception
     */
    public function index(Request $request, UserRepository $userRepository, \Swift_Mailer $mailer) {
        $data = json_decode($request->getContent(), true);
        $user = $userRepository->findOneBy(['email' => $data['email']]);
        if ($user != null) {
            $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            $newPassword = array();
            $alphaLength = strlen($alphabet) - 1;
            for ($i = 0; $i < 8; $i++) {
                $n = rand(0, $alphaLength);
                $newPassword[$i] = $alphabet[$n];
            }
            $passwordGenerate = implode($newPassword);
            $message = (new \Swift_Message('Nouveau mot de passe'))
                    ->setFrom('bartusiak17@gmail.com')
                    ->setTo($user->getEmail())
                    ->setBody("New Password :<b>$passwordGenerate</b>", 'text/html');
            if ($mailer->send($message)) {
                $passwordGenerate = password_hash($passwordGenerate,PASSWORD_BCRYPT);
                $user->setPassword($passwordGenerate);
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                return $this->handleView($this->view(['message' => 'ok'],Response::HTTP_OK));
            } else {
                return $this->handleView($this->view(Response::HTTP_INTERNAL_SERVER_ERROR));
            }
        } else {
            return $this->handleView($this->view(['message' => 'utilisateur inconnu'],Response::HTTP_OK));
        }
    }
}
