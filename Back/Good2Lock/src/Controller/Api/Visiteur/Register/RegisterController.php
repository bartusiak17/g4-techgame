<?php
/**
 * Created by PhpStorm.
 * User: Nicolas Bartusiak
 * Date: 14/02/2020
 * Time: 21:21
 */

namespace App\Controller\Api\Visiteur\Register;


use App\Entity\City;
use App\Entity\User;
use App\Entity\ZipCode;
use App\Form\UserRegisterType;
use App\Repository\CityRepository;
use App\Repository\UserRepository;
use App\Repository\ZipCodeRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/v1/public/compte")
 * Class RegisterController
 * @package App\Controller\Api\Visiteur\Register
 */
class RegisterController extends AbstractFOSRestController {

    private $userRepository;

    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * Vérifie si l'adresse mail existe.
     * @Rest\Post("/verifier/email")
     */
    public function checkAdress(Request $request) {
        $data = json_decode($request->getContent(), true);
        $emailCheck = $data['email'];
        if ($emailCheck != null) {
            $message = 'ok';
            if ($this->checkAdressMail($emailCheck)) {
                $message = 'L\'adresse mail est déja utilisée';
            }
            return $this->handleView($this->view(['message' => $message], Response::HTTP_OK));
        } else {
            return $this->handleView($this->view(null, Response::HTTP_BAD_REQUEST));
        }
    }

    /**
     * Vérifie si l'adresse mail existe
     * @param $email
     * @return bool
     */
    private function checkAdressMail($email) {
        if ($this->userRepository->findOneBy(['email' => $email])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @Rest\Post("/verifier/codeparrainage")
     * Vérifie le code de parrainage existe
     * @param Request $request
     */
    public function checkSponsorship(Request $request) {
        $data = json_decode($request->getContent(), true);
        $sponsorship = $data['sponsorshipCode'];
        if ($sponsorship != null) {
            $message = 'ok';
            if (!$this->userRepository->findOneBy(['is_deleted' => false, 'is_activated' => true, 'is_validated_by_modo' => true, 'sponsorship_code' => $sponsorship])) {
                $message = 'Code de parrainage incorrect';
            }
            return $this->handleView($this->view(['message' => $message], Response::HTTP_OK));
        } else {
            return $this->handleView($this->view(null, Response::HTTP_BAD_REQUEST));
        }
    }

    /**
     * @Rest\Post("/inscription")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function inscription(Request $request, CityRepository $cityRepository, ZipCodeRepository $zipCodeRepository, \Swift_Mailer $mailer) {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);
        $message = "Une erreur c'est produite. Merci de bien vouloir recommencer !";
        if ($this->checkAdressMail($data['email'])) {
            return $this->handleView($this->view(['statut' => "Adresse email déjà utilisée"], Response::HTTP_OK));
        }
        if ($data['city'] != null && $data['zipCode'] != null) {
            $City = $cityRepository->findOneBy(['name' => $data['city']]);
            if (!$City) {
                $city = new City();
                $city->setName($data['city']);
                $em->persist($city);
                $em->flush();
                $City = $cityRepository->findOneBy(['name' => $data['city']]);
            }
            $data['city'] = $City->getId();

            $zipCode = $zipCodeRepository->findOneBy(['zipcode' => $data['zipCode']]);
            if (!$zipCode) {
                $zipCode = new ZipCode();
                $zipCode->setZipcode($data['zipCode']);
                $em->persist($zipCode);
                $em->flush();
                $zipCode = $zipCodeRepository->findOneBy(['zipcode' => $data['zipCode']]);
            }
            $data['zipCode'] = $zipCode->getId();
            $user = new User();
            $form = $this->createForm(UserRegisterType::class, $user);
            $form->submit($data);
            if ($form->isValid() && $form->isSubmitted()) {
                $user = $this->initialiseUser($user);
                if ($this->sendMailValidationCompte($mailer, $user->getEmail())) {
                    $em->persist($user);
                    $em->flush();
                    $message = "ok";
                    return $this->handleView($this->view(['message' => $message], Response::HTTP_OK));
                } else {
                    return $this->handleView($this->view(['message' => $message], Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }
        }
        return $this->handleView($this->view(['message' => $message], Response::HTTP_OK));
    }

    /**
     * Initialise le User
     * @throws \Exception
     */
    private function initialiseUser(User $user) {
        $user->setCreatedAt(new \DateTime());
        $user->setCreditsNumber(0);
        $user->setSponsorshipCode($this->generateCodeSponsorship());
        $user->setIsActivated(false);
        $user->setRoles(["ROLE_USER"]);
        $user->setIsValidatedByModo(false);
        $user->setIsDeleted(false);
        $user->setPassword(password_hash($user->getPassword(), PASSWORD_BCRYPT));
        return $user;
    }

    /**
     * Génère un code de parrainage
     * @return string
     */
    private function generateCodeSponsorship() {
        $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $codeSponsorship = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 5; $i++) {
            $n = rand(0, $alphaLength);
            $codeSponsorship[$i] = $alphabet[$n];
        }
        $codeSponsorshipGenerate = implode($codeSponsorship);
        return $codeSponsorshipGenerate;
    }

    /**
     * Envoie un email au client pour qu'il valide son compte
     */
    private function sendMailValidationCompte(\Swift_Mailer $mailer, $email) {
        $message = (new \Swift_Message('Validation de votre compte'))
                ->setFrom("bartusiak17@gmail.com")
                ->setTo($email)
                ->setBody("<a href='http://localhost:8000/api/v1/public/compte/validerCompte/$email'>Cliquer sur ce lien pour valider votre compte pour valider votre compte</a>", 'text/html');
        if ($mailer->send($message)) {
            return true;
        }
        return false;
    }

    /**
     * @Rest\Route("/validerCompte/{email}")
     */
    public function activateAccount($email) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->userRepository->findOneBy(['email' => $email]);
        $user->setIsActivated(true);
        $em->persist($user);
        $em->flush();
        return new JsonResponse("Bienvenue sur Good2Loc");
    }

}
