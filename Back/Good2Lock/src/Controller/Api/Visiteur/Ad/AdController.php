<?php
/**
 * Created by PhpStorm.
 * User: Nicolas Bartusiak
 * Date: 31/01/2020
 * Time: 22:53
 */

namespace App\Controller\Api\Visiteur\Ad;

use App\Repository\AdRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route("/api/v1/public")
 * Class AdController
 * @package App\Controller\Api\Visiteur\Ad
 */
class AdController extends AbstractFOSRestController {

    /**
     * @var AdRepository
     */
    private $adRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(AdRepository $adRepository, EntityManagerInterface $em) {
        $this->adRepository = $adRepository;
        $this->em = $em;
    }

    /**
     * @Rest\Get("/annonce/consulter/{id}", requirements={"id": "[0-9]*"})
     * @param $id
     * @return Response
     * @throws \Exception
     */
    public function show($id) {
        $ad = $this->adRepository->findOneBy(['is_activated' => true, 'is_validated_by_modo' => true, 'id' => $id]);
        if (!$ad) {
            return $this->handleView($this->view(['status' => 'ok', 'messsage' => 'ad not found'], Response::HTTP_NOT_FOUND));
        }
        return $this->handleView($this->view($ad, Response::HTTP_OK));
    }

}
