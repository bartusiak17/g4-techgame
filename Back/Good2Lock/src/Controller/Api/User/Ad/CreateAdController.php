<?php

namespace App\Controller\Api\User\Ad;

use App\Entity\Ad;
use App\Entity\Category;
use App\Entity\City;
use App\Entity\PicturesAd;
use App\Entity\SubCategorie;
use App\Entity\User;
use App\Entity\ZipCode;
use App\Repository\CategoryRepository;
use App\Repository\CityRepository;
use App\Repository\SubCategorieRepository;
use App\Repository\UserRepository;
use App\Repository\ZipCodeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreateAdController extends AbstractFOSRestController {


    private $serializer;

    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Rest\Post("api/private/v1/annonce/creer")
     * @param Request $request
     * @param CityRepository $cityRepository
     * @param ZipCodeRepository $zipCodeRepository
     * @param SubCategorieRepository $subCategorieRepository
     * @param UserRepository $userRepository
     * @return Response
     * @throws Exception
     */
    public function index(Request $request, CityRepository $cityRepository, ZipCodeRepository $zipCodeRepository, SubCategorieRepository $subCategorieRepository, UserRepository $userRepository) {
        $ad = new Ad();
        $em = $this->getDoctrine()->getManager();
        $information = $request->getContent();
        $data = $this->serializer->deserialize($information, 'App\Entity\Ad', 'json');

        $debut = $data->getStartDate();
        $fin = $data->getEndDate();
        $cityName = $data['city'];
        $zipCode = $data['zipCode'];
        $sub = $data['subCategorie'];

        $picture = new PicturesAd();

        // check urls
        $imgCollection = new ArrayCollection();
        if (!empty($json['urlimage'])){
            foreach ($json['urlimage'] as $urlVar){
                $imgCollection->add($urlVar);
            }
        }

        $city = $cityRepository->findOneBy(['name' => $cityName]);
        if (!$city) {
            $cityInsert = new City();
            $cityInsert->setName($cityName);
            $em->persist($cityInsert);
            $em->flush();
            $city = $cityRepository->findOneBy(['name' => $cityName]);
        }


        $codePostal = $zipCodeRepository->findOneBy(['zipcode' => $zipCode]);
        if (!$codePostal) {
            $zipCodeInsert = new ZipCode();
            $zipCodeInsert->setZipcode($zipCode);
            $em->persist($zipCodeInsert);
            $em->flush();
            $codePostal = $zipCodeRepository->findOneBy(['zipcode' => $zipCode]);
        }
        $sousCategorie = $subCategorieRepository->findOneBy(['name' => $sub]);
        $user = $userRepository->find($this->getUser()->getId());

        if ($user != null && $sousCategorie != null) {
            if (new \DateTime() <= $debut && $debut <= $fin) {

                $ad->setTitle($data->getTitle())
                        ->setDescription($data->getDescription())
                        ->setStartDate($debut)
                        ->setEndDate($fin)
                        ->setPrice(floatval($data->getPrice()))
                        ->setIsActivated(false)
                        ->setIsValidatedByModo(false)
                        ->setIdUser($user)
                        ->setIdCity($city)
                        ->setIdZipCode($codePostal)
                        ->setIdSubcategorie($sousCategorie);

                $em->persist($ad);
                $em->flush();

                foreach ($imgCollection as $v) {
                    $picture->setUrl($v)
                            ->setIdAd($ad);
                    $em->persist($picture);
                    $em->flush();

                    $picture = new PicturesAd();
                }
                return $this->handleView($this->view($ad, Response::HTTP_CREATED));
            }
        }
        return $this->handleView($this->view(['satus' => 'ko', 'Message' => 'data not valid'], Response::HTTP_NOT_ACCEPTABLE));
    }

    /**
     * @Rest\Get("api/private/v1/annonce/category")
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    public function showCat(CategoryRepository $categoryRepository) {

        $categoryCollection = $categoryRepository->findBy(['is_activated' => true]);
        if (!empty($categoryCollection)) {
            return $this->handleView($this->view($categoryCollection, Response::HTTP_ACCEPTED));
        } else {
            return $this->handleView($this->view(['status' => 'ko', 'Message' => 'data not found'], Response::HTTP_NO_CONTENT));
        }

    }

    /**
     * @Rest\Get("api/private/v1/annonce/subCateg")
     * @param SubCategorieRepository $subCategorieRepository
     * @return Response
     */
    public function showSubCat(SubCategorieRepository $subCategorieRepository) {
        $listSub = $subCategorieRepository->findBy(['is_activated' => true]);
        if (!empty($listSub)) {
            return $this->handleView($this->view($listSub, Response::HTTP_ACCEPTED));
        } else {
            return $this->handleView($this->view(['status' => 'ko', 'Message' => 'data not found'], Response::HTTP_NO_CONTENT));
        }

    }


}
