<?php

namespace App\Controller\Api\User\Account;

use App\Repository\UserRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AccountInformationsController
 * @package App\Controller\Api\User\Account
 */
class AccountInformationsController extends AbstractFOSRestController {

    /**
     * @Rest\Get("api/v1/private/compte/informationsCompte")
     */
    public function searchDataUser(UserRepository $userRepository) {
        $idUser = $this->getUser()->getId();
        if ($idUser) {
            $user = $userRepository->findOneBy(['is_deleted' => false, 'id' => $idUser]);
            return $this->handleView($this->view($user, Response::HTTP_OK));
        } else {
            return $this->handleView($this->view(['status' => $idUser], Response::HTTP_NOT_FOUND));
        }
    }
}
