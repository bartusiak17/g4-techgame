<?php
/**
 * Created by PhpStorm.
 * User: Nicolas Bartusiak
 * Date: 31/01/2020
 * Time: 22:53
 */

namespace App\Controller\Api;

use App\Entity\BlobTest;
use App\Entity\Test;
use App\Entity\User;
use App\Form\TestType;
use App\Repository\BlobTestRepository;
use DateTime;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class exampleApiController
 * @package App\Controller\api
 */
class exampleApiController extends AbstractFOSRestController {

    /**
     * Lists all test.
     * @Rest\Get("/api/private/v1/test")
     * @return Response
     */
    public function getTestAction() {
        $userrepository = $this->getDoctrine()->getRepository(User::class);
        $json = $userrepository->findAll();
        return $this->handleView($this->view($json));
    }

    /**
     * Lists all test.
     * @Rest\Get("/api/private/v1/test")
     * @return Response
     */
    public function testPrivate() {
        $userrepository = $this->getDoctrine()->getRepository(User::class);
        $json = $userrepository->findAll();
        return $this->handleView($this->view($json));
    }


    /**
     * Create test.
     * @Rest\Get("api/private/v1/testpost")
     * @param Request $request
     * @return Response
     */
    public function postTestAction(Request $request) {
        /*$data = json_decode($request->getContent(), true);
        $i=0;
        foreach ($data["tableaudate"] as $tableaudate){
            foreach ($tableaudate as $date){
                if($i == 1){
                    return $this->handleView($this->view(['status' => 'ok', 'test' => $date], Response::HTTP_CREATED));
                } else {
                    $i++;
                }

            }
        }*/
        /*        return $this->handleView($this->view(['status' => 'ok', 'test' => $data["date"]], Response::HTTP_CREATED));*/
        return $this->handleView($this->view(['status' => 'ok', 'test' => $this->getUser()->getId()], Response::HTTP_CREATED));

    }

    /**
     * Create test.
     * @Rest\Post("api/v1/public/test/blob")
     * @param Request $request
     * @return Response
     */
    public function testBlobExample(Request $request) {
        $data = json_decode($request->getContent(), true);
        /** @var UploadedFile $img */
        $file = $request->files->get('file');
        if ($file) {
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            // this is needed to safely include the file name as part of the URL
            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
            $newFilename = '' . $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();
            // Move the file to the directory where brochures are stored
            try {
                $file->move(
                        $this->getParameter('test_directory'),
                        $newFilename
                );

            } catch (FileException $e) {
                return $this->handleView($this->view(['status' => 'erreurUpload'], Response::HTTP_CONFLICT));
            }
            return $this->handleView($this->view(['status' => 'ok', "ouraUploadDeFichier"], Response::HTTP_OK));
        }
/*        return $this->handleView($this->view(['status' => 'ok', "oura"], Response::HTTP_OK));*/
       /* if ($request->files) {
            return $this->handleView($this->view($request->files->count(), Response::HTTP_OK));
        }*/
        return $this->handleView($this->view($request->getContent(), Response::HTTP_INTERNAL_SERVER_ERROR));

    }
}
