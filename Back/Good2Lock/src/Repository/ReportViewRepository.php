<?php

namespace App\Repository;

use App\Entity\ReportView;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ReportView|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReportView|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReportView[]    findAll()
 * @method ReportView[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReportViewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReportView::class);
    }

    // /**
    //  * @return ReportView[] Returns an array of ReportView objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReportView
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
