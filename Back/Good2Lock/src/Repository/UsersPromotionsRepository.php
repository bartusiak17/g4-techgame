<?php

namespace App\Repository;

use App\Entity\UsersPromotions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UsersPromotions|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersPromotions|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersPromotions[]    findAll()
 * @method UsersPromotions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersPromotionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsersPromotions::class);
    }

    // /**
    //  * @return UsersPromotions[] Returns an array of UsersPromotions objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsersPromotions
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
