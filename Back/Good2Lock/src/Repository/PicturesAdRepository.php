<?php

namespace App\Repository;

use App\Entity\PicturesAd;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PicturesAd|null find($id, $lockMode = null, $lockVersion = null)
 * @method PicturesAd|null findOneBy(array $criteria, array $orderBy = null)
 * @method PicturesAd[]    findAll()
 * @method PicturesAd[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PicturesAdRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PicturesAd::class);
    }

    // /**
    //  * @return PicturesAd[] Returns an array of PicturesAd objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PicturesAd
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
