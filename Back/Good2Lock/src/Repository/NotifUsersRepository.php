<?php

namespace App\Repository;

use App\Entity\NotifUsers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method NotifUsers|null find($id, $lockMode = null, $lockVersion = null)
 * @method NotifUsers|null findOneBy(array $criteria, array $orderBy = null)
 * @method NotifUsers[]    findAll()
 * @method NotifUsers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotifUsersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NotifUsers::class);
    }

    // /**
    //  * @return NotifUsers[] Returns an array of NotifUsers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NotifUsers
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
