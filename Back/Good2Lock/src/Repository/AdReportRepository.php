<?php

namespace App\Repository;

use App\Entity\AdReport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AdReport|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdReport|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdReport[]    findAll()
 * @method AdReport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdReportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdReport::class);
    }

    // /**
    //  * @return AdReport[] Returns an array of AdReport objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AdReport
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
