<?php

namespace App\Repository;

use App\Entity\PromotionAds;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PromotionAds|null find($id, $lockMode = null, $lockVersion = null)
 * @method PromotionAds|null findOneBy(array $criteria, array $orderBy = null)
 * @method PromotionAds[]    findAll()
 * @method PromotionAds[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromotionAdsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PromotionAds::class);
    }

    // /**
    //  * @return PromotionAds[] Returns an array of PromotionAds objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PromotionAds
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
