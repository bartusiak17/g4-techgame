<?php

namespace App\Repository;

use App\Entity\OpinionUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method OpinionUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method OpinionUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method OpinionUser[]    findAll()
 * @method OpinionUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OpinionUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OpinionUser::class);
    }

    // /**
    //  * @return OpinionUser[] Returns an array of OpinionUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OpinionUser
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
